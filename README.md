# FF14

Repo for various FF14 files: Macros, configs, small helper programs and such.
Gonna add stuff her as i get across it when cleaning up my XIV folder.

## [Macros](/macros)
Various ~~crafting~~ (moved to [crafting](/crafting)) combat (legacy/joke/nostalgia mostly) macros.

## [Crafting](/crafting)
* Various on-patch macros and stuff for crafting in their respective patch's file,
  * [Crafting (7.x - Dawntrail).md](/crafting/Crafting%20%287.x%20-%20Dawntrail%29.md)
  * [Crafting (6.x - Endwalker).md](/crafting/Crafting%20%286.x%20-%20Endwalker%29.md)
  * [Crafting (5.x - Shadowbringers).md](/crafting/Crafting%20%285.x%20-%20Shadowbringers%29.md)
* [Diadem Expert Crafting.md](/crafting/Diadem%20Expert%20Crafting.md)
  * Information for diadem expert crafting (achievements, titles) and 14-16 steps macros for use in Dawntrail.
* [Resplendent Expert Crafting.md](crafting%2FResplendent%20Expert%20Crafting.md)
  * Information for resplendent tool expert crafting, an 18-step "one macro fits all" macro and specialized macros for each component.

## [Checklists](/checklists)
* [Leve Checklist.ods](/checklists/Leve Checklist.ods)
    * Checklist for all leves, sorted by the in-game Journal order + completion date estimate. _2024-08-09: Updated for 7.0._
* [Custom Delivery Tracker.ods](/checklists/Custom Delivery Tracker.ods)
    * Counter/Tracker for completed custom deliveries and *The Customer Is Always Right* achievements + completion date estimate (individual NPC and total).

## [Fishing](/fishing)
* [Diadem Fishing Map.png](/fishing/Diadem Fishing Map.png)
    * A map depicting all fishing holes in Diadem, including their respective umbral weather.
* [Diadem.md](/fishing/Diadem.md)
    * Contains notes/tips/macros for Diadem fishing (achievements).
* [carbuncleplushy.json](/fishing/carbuncleplushy.json) 
    * Personal JSON export of [carbuncleplushy Fish Tracker](https://ff14fish.carbuncleplushy.com/) data.

## [Deep Dungeon](/deep dungeon)
* Collection of my personal notes on [Deep Dungeon](/deep dungeon/README.md) and specific instances ([Eureka Orthos](/deep dungeon/Eureka Orthos.md), [Heaven-on-High](/deep dungeon/Heaven-on-High.md), [Palace of the Dead](/deep dungeon/Palace of the Dead.md))

## Various files

* [AntiAfkKick.kt](/AntiAfkKick.kt)
    * May prevent the automatic AFK kick added in 6.0. Just something I threw together quickly during 6.0 launch period. No guarantees it still works and that SE won't give you a warning or ban for using it.
    * You'll have to build it yourself ([Kotlin compiler](https://kotlinlang.org/docs/command-line.html)) with: `kotlinc AntiAfkKick.kt -include-runtime -d AntiAfkKick.jar`
    * And then run it with: `java -jar AntiAfkKick.jar`
    * By default, it'll press any three of the following buttons (W, A, S, D, B, N, C) 0.2~1.2 seconds after execution and then at random intervals (ranging from 1 minute to 20 minutes), until you manually kill the process.
    * If you changed any keybinds for menues or movement buttons you'll have to adjust that yourself in the file. Modifiers not currently supported.
* [Island Sanctuary.md](/Island Sanctuary.md)
    * Personal notes, protips and resource gathering routes for Island Sanctuary.
* [Linux.md](/Linux.md)
   * Troubleshooting/Hints for various Linux specific issues/topics.

## Snippets
Some WIP stuff may or may not be found at: https://gitlab.com/fellomen/ff14/-/snippets

## Links to useful stuff
### Community things
* [Garland Tools DB](https://garlandtools.org/db/): Database and crafting lists.
* [Lulu’s Ocean Fishing](https://ffxiv.pf-n.co/ocean-fishing): Shows upcoming ocean fishing voyages, a list of fishes and how to get them, as well as info on the achievement routes.
* [Fish Tracker by Carbuncle Plushy](https://ff14fish.carbuncleplushy.com/): Compact tracker for big fish. Good filtering options and allows exporting/importing of your progress as JSON.
* [Faux Hollows Solver](https://sturalke.github.io/FauxHollowsProbabilisticSolver/): Helps with finding swords/chest in faux hollows.
* [Eureka Tracker](https://ffxiv-eureka.com/): Create and share NM trackers for your instance. Logogram tracker as well.
* [FFXIV Gardening](https://www.ffxivgardening.com/index.php): Resources for various gardening topics: Crossbreeding + strategies, pot flower colors, etc.
* [Lalachievements](https://lalachievements.com/): Overall progress tracker. Doesn't require a discord account for login, login required for manual collection tracking. Has a pretty gud levequest tracker if you need that.
### Official things
* [Archive of all patch notes](https://na.finalfantasyxiv.com/lodestone/special/patchnote_log/)
