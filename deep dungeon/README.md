# Deep Dungeon

Just some notes I've been taking during my attempts to clear solo HoH/PotD and soon EO.

## General
- While progging never be afraid to use a good pomander (that you wanted to keep for a later floor) to get you out of a hairy situation. Nothing is worse than dying with a full set of pomanders. And even if you don't end up clearing getting the experience is always worth it.
- You can (and should) use food in Deep Dungeon. You mainly want it for the VIT so any will do, but I think people prefer to go for DH/SS food.
- You can also use stat potions, do keep in mind though that they will lock you out of HP-Potions, but can be very useful on tanks.

## Dungeon specific READMEs:
- [Palace of the Dead](Palace of the Dead.md)
- [Heaven-on-High](Heaven-on-High.md)
- [Eureka Orthos](Eureka Orthos.md)

## External Links
- [Angelusdemonus Youtube](https://www.youtube.com/@Angelusdemonus): Has tons of uploads of solo PotD and HoH clears. Filled to the brim with knowledge and some "tutorial runs" where he takes the time to go into more details too.
- [DEEP DUNGEONS Discord](https://discord.gg/deepdungeons)
- [Maygi's Handbook for HoH 1-100](https://docs.google.com/document/d/1YVBSTOgJO-xOAB6YyKZEZRikjXFPle6Ihf_E7VdmQnI/edit#heading=h.ghuaptxy0myc)
- [Maygi's Handbook for Potd 1-200](https://docs.google.com/document/d/e/2PACX-1vQpzFuhmSwTXuZSmtnKLNgQ0nRhumCFaB8NvCXFXSjrBHPRT5lXY8jMR4RaCK1aNfcl_G5ph5DNNwfl/pub)
