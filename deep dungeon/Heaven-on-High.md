# Heaven-on-High

## General
- Attempt to check as many rooms/chests as possible. It might cost some time but you may just find the one pomander that will save your ass.
    - Gold chests: Unless you're not missing any pomanders always check those. Try to check as many chests as possible before starting to clear out the floor, an extra petrify/frailty/etc. can save you a lot of time.
    - Silver chests: When you're at 3/3 magicite you can skip out those if you already cleared for the exit.
    - Bronze chests: On higher floors you can skip those if they could spell trouble to get to, but potsherds are always nice to have.
- Always respect AoEs and gaze mechanics. Better to lose 1 or 2 GCDs uptime than wipe to being turned into a piggy and eaten by a Centaur.
- Mimics:
    - F01-30: Bronze chests, can be interrupted with knockbacks, stuns, interrupted.
    - F31-60: Silver chests, can be interrupted with stuns and interrupts.
    - F61-99: Gold chests, can only be interrupted with interrupts (`Interject`, `Head Graze`)
    - Consider using a `Pomander of Witching` to get yourself more time to burn down a mimic.
    - Handling Mimics when (planning to) use frailty or petrification (or magicite):
        - Beforehand: Check as many chests as possible to bait out potential mimics (only bait to 1 mimic with frailty)
        - After usage: Avoid opening any chests that could spawn a mimic as it won't be debuffed/petrified. 
    - Fun fact (but not all that useful): You can mimic-check bronze chests by trying to open them while jumping/casting. Source: https://www.reddit.com/r/ffxiv/comments/96frze/hoh_100_mimic_detection_for_floors_130/
- Try to enter floor x6 with >30 minutes to spare, if you're lower than that consider using some timesavers.
- Pomanders of Raising stop droping at floor 79.
    - Be proactive with popping them while progging/you're unsure if you can handle yourself on a specific floor.
    - Definitly recommend popping instantly starting from 81+ as stuff might surprise you with how hard it can hit.
- On higher floors do not respond to text messages if you're not 100% safe (or it's absolutely necessary), mobs with out of combat targeted AoEs might catch you off guard. I died to an elephant throwing a rock at me while I was responding to a friend.
- On lower floors (up to 50/60 depending on what you're comfortable with) you can be very liberal with your pomander usage to get the most out of their time saving potential while not overcapping (ie no item floors, flights on the last floor, etc.). Starting with 51/61 you want to be more conservative to ensure you have a full set of pomanders going into the tougher floors.
- If you start a fresh 1-100 run, I recommend you extract one grip with your aetherpool (assuming you are at 99/99). From my experience you'll reach 89/89 -> 99/99 at floor 41-50 (typically around F44).

## Floor 31-40
- Boss fight: Knockback can be negated with shields.

## Floor 51-60
- The green ADS-like mob (Maruishi, proximity aggro) can be pulled together with other mobs, as they will only shoot a telepgrahed laser AoE at you.

## Floor 61-70
- Boos fight: You can shield the `Hound out of Hell` to avoid the stun and gain 1-2 extra GCDs. However if you're not too comfortable with this fight yet, I recommend just taking the stun. It's not that uncommon for people to negate the stun and then walk out of the puddle only to be killed by `Devour` due to not paying enough attention.

## Floor 71-80
- Use your flights here to save some time (especially on non-DPS) and try to avoid using petris/magicite on this set of floors.
- F71-73: You can do Sight + Landmine plays here pretty well.

## Floor 81-90
- You can start this set of floors with a `Strength + Frailty` to get through 81 and following floors faster to give you a speedy start into the floor set. Do not open chests while `Frailty` is active (potential mimics won't be debuffed).

## Floor 91-100
- Start with a strength and steel, be very proactive with their usage here. If you did save your floor wipes you'll only have to fight through 3-5 floor.
- Try to pick out white mages and gozus.
- Do not be afraid to use a floor wipe on 92/93 if the debuffs are bad, you can still get a not-debuffed floor later on.

## Job specific

### WAR
- Assuming you have the aetherpool level/strength for it, you can delete mobs with IR without `Storm's Eye` active up to ~F50.
- Use Tomahawk to navigate while doing damage (works quite well on low floors).
- Use your gap closer to cover distance (if it's safe to do so (Sight/Safety) or just low floors in general), very helpful on Petrification too.



## Clear

![HoH clear screenshot](img/FINAL_FANTASY_XIV_1677090061.jpg)


