# Palace of the Dead


## General
- If you're not too comfortable about early bosses it's best to clear out the early floors completely so you don't fall behind on levels.
  - F10 boss: Lv 20
  - F20 boss: Lv 35
  - F30 boss: Lv 49
  - F40 boss: Lv 59
  - F50 boss: Lv 60
