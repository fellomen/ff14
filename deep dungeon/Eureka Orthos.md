# Eureka Orthos

## General
- Consider extacting grips when starting a new run (assuming you are at 99/99).
    - For solo you can extract 1 safely, 2 if you feel comfortable.
    - For a group run you can get away with 3 as well (if your party isn't too strict on it)

### Dread Beasts
- Give 30min long buffs based on which type you kill:
    - Lamia Queen: HP regen (2k HP per tick)
        - Will use a stacking HP regen buff on herself
    - Demi-Cochma: Vuln Down 
        - Will use a stacking vuln down buff on itself
    - Meracydian Clone: Damage Up
        - Will cast `Berserk` to give itself a stacking damage up debuff (can be interrupted)
        - When getting to low HP will start casting meteor.

Tips for killing these:
- If you don't feel comfortable attempting it and it's not blocking your way, consider just skipping it.
- Pulling it with `Pomander of Storms` gives you an easy kill.
- You can also use one or multiple `Pomander of Witching` to stop their stacking buffs.
    - When using Witching consider that they might still hit very hard if they get transformed into an Imp.
- You can also use landmines to quickly remove a huge chunk of their HP, be wary of possible auto attacks though (good combination with a witching).

### Demiclones
- Good to speed up boss fights: `Onion Knight > Doga > Unei` in terms of DPS, but Unei will help you save on potions with her heals compared to Doga, she may even allow you to not use a steel/potions at all.
- Otherwise be proactive with them (keep at 2), especially on (somewhat) annoying floors/rooms, or you'll most likely end up with a lot of wasted demiclones at the end of floors. All 3 of them may help you get through bad debuffs without having to use a `Pomander of Serinity`, especially Unei is good to use on no auto-heal floors and/or when afflicted by pox.
- *Each* person in the party can have *one* demiclone summoned at the same time.
    - So for solo you are limited to 1, for a light party you can have up to 4 up at the same time.
    - Once you have a clone summoned you can not replace it, so if you have 2 silver chests close to each other, you could check both to see which on you rather have in your inventory (mostly applies to solo).
- Onion Knight: Big boy DPS + emergency heals
- Unei: Puts Stoneskin on you + heals + DPS
- Doga: Casts AoE petrify on enemies (works on mimics too) + DPS

### Pomander usage
- `Pomander of Storms`: Reduces all mobs on the floor's HP to 1.
    - Normally you should try to pull as much as possible and then use Storms to get as many free kills as possible.
    - On a floor with `Auto-Heal Disabled` debuff: You can just freely pop it and all mobs will not be able to recover HP. 
        - Remember to not pop a serenity afterwards. 
        - Hint: Try to bait out some mimics first before using it, at least if there's any easy to reach chests nearby.
        - Works well in combination with a `Fortune`.
    - These drop fairly often so don't be afraid to keep them at 1 or 2, to make your life easier. Use them to kill Dread Beasts or solve luring traps, and definitly use them on auto-heal disabled debuff floors for quick clears.
- `Pomander of Dread`: Combines Pomander of Rage (skill 1) and Pomander of Lust (skill 2) from Palace of the Dead.
    - Useful to clear out treasure rooms, but be wary to not pull too many mobs (proximity/sound) that may catch you in (untelegraphed) AoEs. 
        - Consider using a `Safety` or `Sight` to avoid hairy situations with landmines or luring traps.
        - Also consider popping an `Orthos Potion` right before transforming for some healing. (only really applies to solo)
    - If you can pop it near an exit and get the required kills, you can take the transformation into the next floor for some more easy kills there.
    - Alternatively they are very useful to speed up boss fights with their stacking vuln up debuff skill.
    - These seem to drop fairly rarely (had one 21-100 run with just 1 drop on F49), I would say be more conservative with those and better keep them for bosses you aren't comfortable with or really bad treasure rooms on your way.

### Mimics
- F01-30: Bronze chests, can be interrupted with knockbacks, stuns, interrupts.
    - From my experience drop rates for `Orthos Aetherpool Fragment` in the early floors are abysmal, so I would suggest to skip all bronze chests from floor 1-30 (at least on classes that can't deal with them properly).
- F31-60: Silver chests, can be interrupted with stuns and interrupts.
- F61-99: Gold chests, can only be interrupted with interrupts (`Interject`, `Head Graze`)


## Floor 1-10
- Proximity mobs:
    - Claws
    - Water Sprite
- Sound mobs:
    - Bhoot
    - Thanatos

### Floor 10 boss
- On DPS: Recommened to use Steel for this boss to save potions.
- Lazy facetanking strategy:
    1. Place the boss in one of the small squares surrounded by 4 big squares.
    2. On Authoritative Shriek
        - with 2 mines around it: Move on the "lane" on the opposite side (this also applies to 1-2-3 explosions if your square is between 1 and 3).
        - with 3 mines around it: Move to the free square's inner-most square
        - with 4 mines around it: ¯\\_(ツ)_/¯ better luck next time.

<details>
<summary>Visual description of lazy facetanking strategy</summary>
![Visual description of lazy facetanking strategy](img/eo_f10_facetanking.jpg)
</details>

## Floor 11-20
- Proximity mobs:
    - Microsystem (ADS)
    - Orthosystem β

### Floor 20 boss
- `Order Relay` melee uptime:
    1. Start on the side that is getting tethered first
    2. Stand on the line on the ground in the "big slice" that is free from tethers.
    3. After the first set of Flame Breaths went out you go across the middle and stand on the line again.
- Probably needs Steel on DPS, saves some potions

<details>
<summary>Visual description of Order Relay melee uptime</summary>
![Visual description of order relay melee uptime](img/eo_f20_order_relay.jpg)
</details>

## Floor 21-30
- Vanara have an untelegraphed close-range circle AoE that will do knockback + long stun (dangerous).
- Dragon patrols have a huge untelegraphed frontal cone AoE (Swinge)

### Floor 30 boss
- Can get away without Steel if you can bring some self-sustain. The boss spends extended times just casting stuff. But there's not much other use for Steel at this point so it's a good use nonetheless.

## Floor 31-40
- Phantom Orthoray (patrol)
    - `Forearming`: Untelegraphed whole area frontal cleave, stay behind.
    - `Atmospheric Displacement`: Untelegraphed circle AoE, stay far.
- Brobinyak: If you stun the `Cold Gaze` it won't do the followup `Body Slam` (melee uptime).
- Proximity mobs:
    - Orthobug
- Sound mobs:
    - Orthospider: Will do an untelegraphed circle AoE that puts Mini debuff on you, which will then end up killing you.
    - Empuse: Walks behind you and does a knockback (with cast time), when you see it move just place yourself between a wall and the Empuse to avoid getting knocked anywhere you don't want to get knocked to.

### Floor 40 boss
- Don't forget it does Twister during the dive bomb.
- On DPS: Can do without Steel as it should be casting most of the time.

## Floor 41-50
- Hedetet (scorpions): Will put a stacking slow debuff on you, but otherwise not too bad
- Gobbue: Stand behind it after it does the suck in, it will do an untelegraphed frontal cone
- Hoarhoud: Abyssal Cry will stun you and then oneshot you.
- Unicorn: Does 3 knockbacks (be sure to have your back against a wall) and then performs an untelegraphed circle AoE
- Proximity enemies:
    - Bergthurs
    - Apa
    - Mudman

### Floor 50 boss
- Thunderous Cold: `In then out`
- Cold Thudner: `Out then in`
- Song of Ice and Thudner: `Out then in`
- Song of Thunder and Ice: `In then out`

## Floor 51-60
- Snails use Ice Spikes (`Gelid Charge`) that will kill you if you attack it with physical attacks (casters are safe as long as you dont activate auto-attacks). Reaper's Harpe is magic damage so can be used, but it will activate your auto-attacks, which will kill you if you are in melee range.
    - Can be stunned, be ready after the first GCD on pull, otherwise just be sure to sheathe your weapon.
- Sharks will do a targeted line AoE -> has to be LoS'ed or it will kill you.
- Yabby will put a heavy on you and then cast a `Tailscrew` AoE on you (puts you to 1HP). If you start moving in a direction when you get the heavy you can outrun `Tailscrew`.
- Sound mobs:
    - Zaratan: `Sewer Water` will cleave frontal first (telegrpahed) and then the back (**untelegraphed**).

### Floor 60 boss
- `Disorienting Groan` is a knockback, can be mitigated with knockback resistance (not with shields).

## Floor 61-70
- Gowrow (patrols) do untelegraphed attacks (frontal cones, etc.)
- Drake: Uses `Smoldering Scales` which will oneshot any **physical** attackers, similar to Gelato's `Gelid Charge`.
- Falak: If anyone is far away from it, it will cast `Electric Cachexia`, which is a huge donut AoE -> stay `In`. Otherwise it only does auto attacks.
    - Note: It also does this when the far away party member is dead at the moment (makes them easy targets if you pull them like that, because they will spend most time casting).


### Floor 70 boss
- `Prenatural Turn`, this also applies to the end of the roar mechanic:
    - When glowing green/blue-ish: `In`
    - If not glowing: `Out`
- `Roar`
    - If you have to be `In`, position yourself close to the one further away crystal inside the target circle for a small safe spot. [Visual guidance](img/eo_f70_roar_in.jpg)
    - If you have to be `Out`, position yourself to either side of the further away crystal.

## Floor 71-80
- Primelephas: Will perform a charge and then a very fast untelegraphed close range AoE (Rear).
- Couerl: Does huge frontal cone AoE (Wild Blast) and back cone AoE (Tail Swipe).
- Thunderbeast (patrol): Spark is `in` (wide range), also has a wind up animation for a close range instakill.
- Bird of Orthos: As always, respect the gaze or it will oneshot you.
    - If one of your party members gets confused, make sure you don't stand near them `Revelation` will cleave around the target.
- Sasquatch: After it eats the bananas, it will do a huge AoE that will oneshot anyone in range, can/must be LoS'ed.


### Floor 80 boss
- Centralized Nerve Gas: Frontal cone. [Standing on the side arrow of the target circle is safe.](img/eo_f80_centralized_nerve_gas.jpg)
- Left-/Rightward Nerve Gas: Angled half arena cleave on the corresponding side of the boss.
    - Tip: Stay in front of it and just dodge slightly to the side it says on the cast bar, also avoids the lasers.
- Nerve gas ring: `In`
- One nerve gas hit is survivable if you use a Steel as DPS.
- Reminder to not step into the boss's circle ring during `Nanospore Jet` (just avoid it in general to be safe).


## Floor 81-90
- Proximity mobs:
    - Corse: `Glass Punch` is an untelegrpahed frontal cone, easy target.
    - Persona: Easy target
    - Gourmand: Same as Gobbue from earlier, does a suck-in and then untelegrpahed frontal cone AoE.
    - Wraith: `Scream` = big circle AoE, be ready to run or use witching.
    - Specter:
        - `Left/Right Sweep` stay on the opposite side of (if you are in front go to the direction the cast says)
        - `Ringing Burst` = `In`
        - `Surrounding Burst` = `Out`
- Sound mobs:
    - Hecteyes: Does a huge circle AoE, can be sprinted out of if you are prepared and/or kiting. Probably better to avoid.
- Easy targets:
    - Abyss
    - Deepeye
    - Ahriman

### Floor 90 boss
- `Salvo Script`:
    - If the eggs are both aiming towards the middle: Stand in the corner between them.
    - If one egg is facing "outwards": Stand on the opposite of it (next to the other one). Alternatively if one egg faces to the side, just go on the side with no egg of the middle facing one.
- As a DPS with Steel you can take 1 beam hit, you can also take a second itself but the following AA might just kill you (this happened to me).

## Floor 91-100
- Orthochimera: Normal `Dragon's/Ram's Voice` and also some conal attacks, prefer avoiding/nuking if possible.
- Easy targets to pick out:
    - Motherbit: Proximity. `Citadel Buster` = untelegrpahed line AoE
    - Orthotaur: normal minotaur mechanics
    - Sphinx: Just make sure to dodge the gaze `Naked Soul` and frontal cone `Swinge`.
    - Zaghnal: Be wary, they cast huge oneshot AoE **when not in combat**, easy once pulled. Jumps on a player (knockback around the player, irrelevant on solo).
    - Fitter: Be wary, they cast huge oneshot AoE **when not in combat**, easy once pulled.
- Sound mobs:
    - Mining Drone: Does 4 knockbacks and then a huge frontal box AoE.
- Proximity mobs:
    - Orthosystem α (egg-formed ADS): Untargeted laser
    - Orthosystem γ (normal ADS): patrols with weird movement, **requires an interrupt for `High Voltage`**
        - `Repelling Canons` = `Out`
        - Other cast (don't remember name) = `In`
        - If you don't have an interrupt for High Voltage you can still walk/run out of it, be ready to move after 1-2 auto-attacks. It's about similar in size to Zaghnal and Fitter AoEs. A Lethargy can help you out as well.
    - Orthodrone: Explodes after a short delay on death (similar to Ice Sprites in earlier floors).
    - Durga

### Floor 99 boss
- `Caliburni` blades shoot out: In front of the boss, and back diagonal left and right. Stand to the sides.
    - Stand angled between the front and side arrow of the target circle.
- If the boss does `Flameforged`, you need to have the ice debuff (either have it from Paradoxum cast or switch via getting hit by the blades). Vice versa if the boss does `Frostforged`, you need the fire debuff.
- `Empty Soul's Calibur = In`
- `Solid Soul's Calibur = Out`
- Safe spots for `Exglacialis` (regardless of where they drop): [Pic/Safespots by Nori](img/eo_f99_exglacialis_by_nori.jpg)

### Floor 100
After defeating the floor 99 boss you still have to reach the `Orthos Tomestone` with your normal instance timer, **even if you had less than 10 minutes left**. Using sprint (no class-specific gap closers) you will need about `1:30` to reach it. So be sure to have more time than that left over.



## Clear

![EO clear screenshot](img/FINAL_FANTASY_XIV_1679245937.jpg)

