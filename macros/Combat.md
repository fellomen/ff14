# Macros for raids and trials

## Rubicante / Mount Ordeals (Extreme)
```
/p ┏━━━━━━━━━━━━━━━━━━━━┓
/p ┃ 《SPREADS》 ┃        《Arch Inferno 》    
/p ┃     ┃   Groups            
/p ┃         ┃             W       +  
/p ┃     ┃             E         +  
/p ┣━━《SWEEPING IMMOLATION》━━━┫
/p ┃                                          (Boss Relative) 
/p ┃                    →  Close        
/p ┃                    →   Far            
/p ┣━ Flamespire ━━━━《NUMBERS》━┫
/p ┃     Flares           ┃   Blue   - Red    
/p ┃  color-coded  ┃  wait  -  7+8 bait first
/p ┃ No flare  in    ┃      
/p ┗━━━━━━━━━━━━━━━━━━━┛
```

## Barbariccia / Storm's Crown (Extreme)
```
/p ┏━━━━━━━━━━━━━━━━━━━━━━━━━┓
/p ┃      Positions      ┃                                                  ┃
/p ┃       ┃  PAIRS: Color coded to markers  ┃
/p ┃           ┃(: 1-A | : 2-B | : 3-C | : 4-D)┃
/p ┃       ┣ ━ ━ ━《ENUM. PAIRS》 ━ ━ ━ ┫
/p ┃      |        ┃    / move CW (Color coded)  ┃
/p ┣ ━ ━ ━ ━ ━ ━ ╋ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ┫
/p ┃ 《TANGLE 》  ┃              《WALL SPREAD》               ┃
/p ┃ STACKS:  -     ┃                              ┃
/p ┃     AOEs:  -     ┃                                  ┃
/p ┣ ━ ━ ━ ━ ━ ━ ╋ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ┫
/p ┃ FLARES: N W-E ┃    《POST-STOMPS PUDDLES》    ┃
/p ┃        STACK: S      ┃    In front of boss (South)  CW  ┃
/p ┗ ━ ━ ━ ━ ━ ━ ┻ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ━ ┛
```


## Not so serious/dated macros for raids, trials and dungeons

### SHIELD OATH
A bit dated nowadays, but I didn't want to delete it anyway.
```
/p    .d8888.     db        db     d888888b  d88888b      db                 d8888b.
/p    88'      YP   88        88           `88'         88'                  88                 88      `8D
/p     `8bo.         88ooo88             88          88ooooo      88                88          88
/p       `Y8b.       88~~~~88             88          88~~~~~         88                 88          88
/p    db        8D  88        88             88          88.                  88booo.     88       .8D
/p     `8888Y'    YP        YP    Y888888P   Y88888P     Y88888P    Y8888D'
/p .
/p        .d88b.             .d8b.       d888888b   db        db
/p     .8P      Y8.        d8' `8b      `~~88~~'      88        88
/p    88          88     88ooo88          88           88ooo88
/p    88          88     88~~~~88          88           88~~~~88
/p   `8b       d8'      88        88          88           88        88
/p       `Y88P'         YP        YP          YP           YP        YP
```

### T9 P4
```
/echo Fire OUT
/echo Thunder
/echo Lunar Dynamo
/echo Fire IN
/echo Thunder
/echo Fire OUT
/echo Thunder
/echo Chariot
/echo Super Nova
/echo Thermionic
/echo Fire IN
/echo Thunder
```

### When you wipe on Deltascape V3.0
```
/p Halicarnassus: How can you not grasp such a simple game? Are you failing on purpose?
```

### ~ 3 minutes of AFK Umbral Soul
For when the raid leader in DRS takes 30 minutes to explain mechanics and/or people have to walk their dog.

You can get some extra time out by removing the `\micon` line and adding another `Umbral Soul`, or alternatively by increasing the the wait duration to 14 seconds, but I wasn't brave enough to try that.
```
/ac "Umbral Soul" <wait.13>
/ac "Umbral Soul" <wait.13>
/ac "Umbral Soul" <wait.13>
/ac "Umbral Soul" <wait.13>
/ac "Umbral Soul" <wait.13>
/ac "Umbral Soul" <wait.13>
/ac "Umbral Soul" <wait.13>
/ac "Umbral Soul" <wait.13>
/ac "Umbral Soul" <wait.13>
/ac "Umbral Soul" <wait.13>
/ac "Umbral Soul" <wait.13>
/ac "Umbral Soul" <wait.13>
/ac "Umbral Soul"
/echo <se.6> Click me again!
/micon Umbral Soul
```