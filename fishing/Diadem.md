# Diadem Fishing

- For more information/guide check out: [Teamcraft Diadem Fishing Guide](https://guides.ffxivteamcraft.com/guide/diadem-fishing-guide).
- All umbral weather specific fish are a **!!!** bite.

## Map
![Diadem Fishing Map](Diadem Fishing Map.png)

## Phase 4
### Fish summary

| Fishing hole + weather | Bait               | Normal fish      | Bite    | Points | Scrips | Notes                                                             |
| :--                    | :--                | :--              | :-:     | --:    | --:    | :--                                                               |
| Windswept Cloudtop     | Mooch Ghost Faerie | Lightning Chaser | **!!!** |  996   |  64    |                                                                   |
| *→ Umbral Levin*       | Mooch Ghost Faerie | Meganeura        | **!!!** | 1078   | 126    |                                                                   |
| | | | | | | |
| Blustery Cloudtop      | Diadem Hoverworm   | Sweatfish        | **!**   |  106   |  17    | Triple/Double Hook                                                |
| *→ Umbral Duststorm*   | Diadem Hoverworm   | Marrella         | **!!!** |  512   |  77    |                                                                   |
| | | | | | | |
| Swirling Cloudtop      | Mooch Ghost Faerie | Little Thalaos   | **!!**  |  911   | 124    |                                                                   |
| *→ Umbral Tempest*     | Mooch Ghost Faerie | Griffin          | **!!!** |  512   |  77    |                                                                   |
| | | | | | | |
| Calm Cloudtop          | Diadem Crane Fly   | Sculptor         | **!!**  |  250   |  17    | Catch 2x Ocean Cloud for Intuition. Triple/Double Hook            |
| *→ Umbral Flare*       | Diadem Crane Fly   | Namitaro         | **!!!** |  542   |  45    |                                                                   |

#### Triple/Double Hook thresholds
| Gathering | TH | DH | GP Efficiency                |
|:---------:|:--:|:--:|:----------------------------:|
| ≥ 3450    |  7 |  4 | 100/fish (TH and DH)         |
| ≥ 2300    |  5 |  3 | 140/fish (TH), 133/fish (DH) |
| < 2300    |  3 |  2 | 233/ifsh (TH), 200/fish (DH) |

### Macros
Macros for quick reminders what to use for phase 4 fishing holes / umbral weathers.
```
/echo ┌─ 【 Calm Cloudtop 】   ☀ Umbral Flare ─────────────────┐
/echo     Bait: Diadem Crane Fly
/echo     Sculptor: with intuition Triple/Double Hook a !! bite at 20+ seconds.  
/echo      Intuition: 2 Ocean Cloud (！at 10-20 seconds). Double Hook possible.
/echo     ☀ Namitaro: !!! bite
/echo └─────────────────────────────────────┘
/micon "Fire"
```

```
/echo ┌─ 【 Blustery Cloudtop 】   ☀ Umbral Duststorm ────────────┐
/echo     Bait: Diadem Hoverworm
/echo     Sweatfish: Triple/Double Hook any ! bite after 14 seconds. 
/echo              TH/DH Gathering Breakpoints: ≥ 2300
/echo     ☀ Marrella: !!! bite
/echo └─────────────────────────────────────┘
/micon "Stone"
```

```
/echo ┌─ 【 Windswept Cloudtop 】   ☀ Umbral Levin ─────────────┐
/echo     Bait:  Any / Mooch Ghost Faerie (Precision Hookset)
/echo     Both !!! bites are mooched and use Powerful Hookset
/echo     Lightning Chaser  ( !!! )－  ☀ Meganeura ( !!! )
/echo └─────────────────────────────────────┘
/micon "Thunder II"
```

```
/echo ┌─ 【 Swirling Cloudtop 】   ☀ Umbral Tempest ─────────────┐
/echo     Bait:  Any / Mooch Ghost Faerie (Precision Hookset)
/echo     Both !!! bites are mooched and use Powerful Hookset
/echo     Little Thalaos  ( !! bite at 18+ seconds) －  ☀ Griffin: !!! bite
/echo └─────────────────────────────────────┘
/micon "Aero"
```

#### Timer macros 
Macros with timer information that make it easier to identify possible fish.
These have both Hook/Cast in them so you only need to press one button. As a side effect unfortunately it will pop up an error message when casting.

#### General purpose
Good stuff line is after which point it's usually safe to use Triple/Double Hook.
```
/macrocancel
/merror off
/ac Hook
/ac Cast
/wait 3
/echo .. 3 <wait.2>
/echo .... 5 <wait.2>
/echo ...... 7 <wait.3>
/echo ........ 10 <wait.2>
/echo .......... 12 <wait.2>
/echo ............ 14 === GOOD STUFF LINE ==== <wait.6>
/echo .............. 20 <wait.5>
/echo ................ 25 <wait.5>
/echo .................. 30
/micon Cast
```

#### Sculptors / Calm Cloudtop
Ocean Clouds should be available at 10 seconds, but I've had some unreliable results with overlapping Ghost Faerie window.
```
/macrocancel
/merror off
/ac Hook
/ac Cast
/wait 11
/echo ※ 11 － Ocean Cloud ! <wait.10>
/echo ※ 20 － Sculptor !! <wait.10>
/micon Cast
```

#### Sweatfish / Blustery Cloudtop
Ocean Clouds should be available at 10 seconds, but I've had some unreliable results with overlapping Ghost Faerie window.
```
/macrocancel
/merror off
/ac Hook
/ac Cast
/wait 14
/echo ※ 14 － Sweatfish. Use Triple/Double Hook on ! bite.
/micon Cast
```

#### Ghost Faeries / Windswept & Swirling Cloudtop
```
/macrocancel
/merror off
/ac Hook
/ac Cast
/wait 11
/echo ※ 11 － Ghost Faerie bite window is over. Recast.
/micon Cast
```



## Phase 2 and Phase 3 titles
Phase 2 and phase 3 fish do not reward any scrips and only a small amount of points.

During umbral weather I would recommend to go for a straight catch umbral fish and switch between the phase 2/3 fishing hole if needed. Mooched umbral fish are in competition with the normal mooched fish, which are not worth Double/Triple Hooking. Alternatively you can also just switch to phase 4 fishing holes during umbral weathers.
If you balance out your progress between the two phases it also shouldn't be an issue to "overcap" on one phase.

The strategy is the same for both phases: Use the respective bait in the respective hole and `Double/Triple Hook` every bite **after** 14 seconds.

### Triple/Double Hook tresholds

| Fish |  Gathering | TH | DH |
| :-:  | :-:  |:-:  |:-:  |
| **Phase 2** <br /> Pterodactyl <br /> Skyfish | ≥ 3075 <br /> ≥ 2075 <br /> < 2075 | 7 <br /> 5 <br /> 3 <br /> | 4 <br /> 3 <br /> 2 <br />  |
| **Phase 3** <br /> Blind Manta <br /> Oscar   |        <br /> ≥ 2075 <br /> < 2075 |   <br /> 5 <br /> 3 <br /> |   <br /> 3 <br /> 2 <br />  |


### Phase 2
Fishing hole: **Buffeted Cloudtop**
| Bait             | Normal fish          | Umbral weather | Umbral fish     |
| :--              | :--                  | :--            | :--             |
| Diadem Crane Fly | Pterodactyl (**!!**) | Umbral Levin   | Rhamphorhynchus |
| Diadem Hoverworm | Skyfish     (**!**)  | Umbral Tempest | Dragon's Soul   |

During *Umbral Flare* and *Umbral Duststorm* you can switch to **Windbreaking Cloudtop** to catch phase 2 umbral fish along phase 3 normal fish.

### Phase 3
Fishing hole: **Windbreaking Cloudtop**
| Bait               | Normal fish          | P3 Umbral weather | P3 Umbral fish |     | P2 Umbral weather | P2 Umbral fish |
| :--                | :--                  | :--               | :--            | :-- | :--               | :--            |
| Diadem Red Balloon | Blind Manta (**!!**) | Umbral Levin      | Cloudshark     |     | Umbral Flare      | Cometfish      |  
| Diadem Crane Fly   | Oscar       (**!**)  | Umbral Tempest    | Archaeopteryx  |     | Umbral Duststorm  | Anomalocaris   |


