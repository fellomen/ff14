# (Probably) Shadowbringers macros
These are (probably) old ShB macros. Most likely won't work due to skill changes, but I wanted to keep them somewhere.

## Shadowbringer's Relic Tools / Skybuilder

### Skysteel
```
/macrolock
/ac Reflect <wait.3>
/ac "Waste Not" <wait.3>
/ac "Basic Touch" <wait.3>
/ac "Basic Touch" <wait.3>
/ac "Basic Touch" <wait.3>
/ac "Basic Touch" <wait.3>
/ac "Waste Not" <wait.3>
/ac "Basic Touch" <wait.3>
/ac "Great Strides" <wait.3>
/ac Innovation <wait.3>
/ac "Byregot's Blessing" <wait.3>
/ac Veneration <wait.3>
/ac Groundwork <wait.3>
/ac Groundwork <wait.3>
```

### Skysteel +1, 2420 craftsmanship, 2231 control, 508 CP, 2 macros
#### Macro 1
```
/macrolock
/ac Reflect <wait.3>
/ac Manipulation <wait.3>
/ac Veneration <wait.3>
/ac "Waste Not II" <wait.3>
/ac Groundwork <wait.3>
/ac Groundwork <wait.3>
/ac "Basic Touch" <wait.3>
/ac "Standard Touch" <wait.3>
/ac "Basic Touch" <wait.3>
/ac "Standard Touch" <wait.3>
/ac "Basic Touch" <wait.3>
/ac "Standard Touch" <wait.3>
/ac "Basic Touch" <wait.3>
/echo >>> NEXT <se.4>
```
#### Macro 2
```
/ac "Standard Touch" <wait.3>
/ac "Great Strides" <wait.3>
/ac "Byregot's Blessing" <wait.3>
/ac Groundwork <wait.3>
/echo >>> DONE <se.4>
```

### Augmented Dragonsung
#### Macro 1
```
/macrolock
/ac "Reflect" <wait.3>
/ac "Manipulation" <wait.3>
/ac "Veneration" <wait.3>
/ac "Waste Not II" <wait.3>
/ac "Groundwork" <wait.3>
/ac "Groundwork" <wait.3>
/ac "Innovation" <wait.3>
/ac "Basic Touch" <wait.3>
/ac "Standard Touch" <wait.3>
/ac "Basic Touch" <wait.3>
/ac "Standard Touch" <wait.3>
/ac "Innovation" <wait.3>
/echo NEXT <se.4>
/micon "wind-up Nidhogg" minion
```
#### Macro 2
```
/macrolock
/ac "Basic Touch" <wait.3>
/ac "Standard Touch" <wait.3>
/ac "Basic Touch" <wait.3>
/ac "Standard Touch" <wait.3>
/ac "Innovation" <wait.3>
/ac "Great Strides" <wait.3>
/ac "Byregot's Blessing" <wait.3>
/ac "Basic Synthesis" <wait.3>
/echo DONE <se.3>
/micon "wind-up Nidhogg" minion
```
### Skysung
This probably uses the Macro 1 from `Augmented Dragonsung`.
#### Macro 2
```
/macrolock
/ac "Basic Touch" <wait.3>
/ac "Standard Touch" <wait.3>
/ac "Basic Touch" <wait.3>
/ac "Standard Touch" <wait.3>
/ac "Innovation" <wait.3>
/ac "Great Strides" <wait.3>
/ac "Byregot's Blessing" <wait.3>
/ac "Observe" <wait.3>
/ac "Focused Synthesis" <wait.3>
/echo DONE <se.3>
/micon "wind-up Nidhogg" minion
```

## 3* 35D
### Version 1
```
/ac "Muscle Memory" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Master's Mend" <wait.3>
/ac "Manipulation" <wait.3>
/ac "Groundwork" <wait.3>
/ac "Waste Not II" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Innovation" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Great Strides" <wait.3>
/ac "Byregot's Blessing" <wait.3>
/ac "Groundwork" <wait.3>
```
### Version 2 - More Progress
```
/ac "Muscle Memory" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Master's Mend" <wait.3>
/ac "Manipulation" <wait.3>
/ac "Groundwork" <wait.3>
/ac "Waste Not II" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Groundwork" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Innovation" <wait.3>
/ac "Basic Touch" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Trained Finesse" <wait.3>
/ac "Great Strides" <wait.3>
/ac "Byregot's Blessing" <wait.3>
```
### Version 3 - More Progress v2
```
/ac "Muscle Memory" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Master's Mend" <wait.3>
/ac "Manipulation" <wait.3>
/ac "Groundwork" <wait.3>
/ac "Waste Not II" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Innovation" <wait.3>
/ac "Basic Touch" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Great Strides" <wait.3>
/ac "Byregot's Blessing" <wait.3>
/ac "Groundwork" <wait.3>
```

### Version ?? - "More Progress"
```
/ac "Muscle Memory" <wait.3>
/ac "Inner Quiet" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Master's Mend" <wait.3>
/ac "Manipulation" <wait.3>
/ac "Groundwork" <wait.3>
/ac "Waste Not II" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Delicate Synthesis" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Innovation" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Great Strides" <wait.3>
/ac "Byregot's Blessing" <wait.3>
/ac "Groundwork"
```

## 4* 35 dura, 626 CP
### Macro 1
```
/ac "Muscle Memory" <wait.3>
/ac "Inner Quiet" <wait.3>
/ac "Manipulation" <wait.3>
/ac "Veneration" <wait.3>
/ac "Waste Not II" <wait.3>
/ac "Groundwork" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Basic Touch" <wait.3>
/ac "Standard Touch" <wait.3>
/ac "Basic Touch" <wait.3>
/ac "Standard Touch" <wait.3>
/ac "Master's Mend"
/echo >>>  NEXT <se.3>
```
### Macro 2
```
/ac "Innovation" <wait.3>
/ac "Basic Touch" <wait.3>
/ac "Great Strides" <wait.3>
/ac "Byregot's Blessing" <wait.3>
/ac "Basic Synthesis" <wait.3>
/ac "Basic Synthesis"
/echo DONE <se.4>
```

## Sub-80 40 dura 100% HQ
```
/macrolock
/ac "Reflect" <wait.3>
/ac "Waste Not" <wait.3>
/ac "Manipulation" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Great Strides" <wait.3>
/ac "Byregot's Blessing" <wait.3>
/ac "Groundwork"
```

## Sub-80 80 dura 100% HQ
```
/macrolock
/ac Reflect <wait.3>
/ac "Waste Not" <wait.3>
/ac Innovation <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Byregot's Blessing" <wait.3>
/ac Groundwork <wait.3>
/ac Groundwork
```

## 4* 70 dura, 602 CP, 2 macros
### Macro 1
```
/macrolock
/ac "Muscle Memory" <wait.3>
/ac "Inner Quiet" <wait.3>
/ac Manipulation <wait.3>
/ac Veneration <wait.3>
/ac "Waste Not" <wait.3>
/ac Groundwork <wait.3>
/ac Groundwork <wait.3>
/ac "Delicate Synthesis" <wait.3>
/ac "Delicate Synthesis" <wait.3>
/ac "Waste Not" <wait.3>
/ac Innovation <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Preparatory Touch"
/echo NEXT <se.4>
```
### Macro 2
```
/macrolock
/ac "Preparatory Touch" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac Innovation <wait.3>
/ac "Great Strides" <wait.3>
/ac "Byregot's Blessing" <wait.3>
/ac "Basic Synthesis"
```

## "Easy Crafts"
```
/ac "Reflect" <wait.3>
/ac "Manipulation" <wait.3>
/ac "Great Strides" <wait.3>
/ac "Innovation" <wait.3>
/ac "Standard Touch" <wait.3>
/ac "Great Strides" <wait.3>
/ac "Standard Touch" <wait.3>
/ac "Great Strides" <wait.3>
/ac "Innovation" <wait.3>
/ac "Standard Touch" <wait.3>
/ac "Great Strides" <wait.3>
/ac "Byregot's Blessing" <wait.3>
/ac "Veneration" <wait.3>
/ac "Groundwork" <wait.3>
/ac "Groundwork" <wait.3>
```

## 3* + 4*, 2 part
### Macro 1 (shared)
```
/ac Reflect <wait.3>
/ac Manipulation <wait.3>
/ac Veneration <wait.3>
/ac "Waste Not II" <wait.3>
/ac Groundwork <wait.3>
/ac Groundwork <wait.3>
/ac Groundwork <wait.3>
/ac Innovation <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Delicate Synthesis" <wait.3>
/ac Innovation <wait.3>
/ac "Delicate Synthesis" <wait.3>
/echo >>> NEXT (3* 70D 597CP Part 2) <se.4>
```
### 3* Macro 2 (597CP)
```
/ac "Delicate Synthesis" <wait.3>
/ac "Great Strides" <wait.3>
/ac "Byregot's Blessing" <wait.3>
/ac "Basic Synthesis"
/echo DONE <se.3>
```
### 4* Macro 2
```
/ac Observe <wait.3>
/ac "Focused Synthesis" <wait.3>
/ac Innovation <wait.3>
/ac "Great Strides" <wait.3>
/ac "Byregot's Blessing" <wait.3>
/ac "Careful Synthesis"
/echo DONE <se.3>
```