# Resplendent tools expert crafting


## Materials & crafting path (assumes 98%+ collectability on each craft)
List for each crafter's and overall needed materials: https://garlandtools.org/db/#list/e8pkwpgvas

30 Material A -> 30 Component A -> 60 Material B \
60 Material B -> 30 Component B -> 60 Material C \
60 Material C -> 30 Component C -> 60 Final Material

## Macros (as of 7.05) - guaranteed 2 materials (98%+ collectability) per craft
Rotation folder on Teamcraft: https://ffxivteamcraft.com/rotation-folder/hgbmVGm4uHNBff7x5k46

###  One macro fits all components (18 steps, 620 CP, 4704+ craftsmanship, 4537+ control)
- Teamcraft rotation: https://ffxivteamcraft.com/simulator/36327/34619/5bejfXHXF6bypZV91Lac

Macro 1
```
/ac "Reflect" <wait.3>
/ac "Waste Not II" <wait.2>
/ac "Manipulation" <wait.2>
/ac "Innovation" <wait.2>
/ac "Preparatory Touch" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Innovation" <wait.2>
/ac "Preparatory Touch" <wait.3>
/ac "Great Strides" <wait.2>
/ac "Trained Perfection" <wait.3>
/ac "Byregot's Blessing" <wait.3>
/ac "Waste Not" <wait.2>
/ac "Veneration" <wait.2>
```
Macro 2
```
/ac "Groundwork" <wait.3>
/ac "Groundwork" <wait.3>
/ac "Groundwork" <wait.3>
```

### Specialized component macros

### Resplendent Component A (15 steps, 568CP, 4872+ craftsmanship, 4602+ control)
Rotation on teamcraft: https://ffxivteamcraft.com/simulator/36311/34603/NxuaXvYa1GMpacRilQa3

```
/ac "Reflect" <wait.3>
/ac "Veneration" <wait.2>
/ac "Waste Not II" <wait.2>
/ac "Groundwork" <wait.3>
/ac "Groundwork" <wait.3>
/ac "Delicate Synthesis" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Immaculate Mend" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Innovation" <wait.2>
/ac "Preparatory Touch" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Great Strides" <wait.2>
/ac "Byregot's Blessing" <wait.3>
/ac "Delicate Synthesis" <wait.3>
```

### Resplendent Component B
No specialized macro implemented yet.

### Resplendent Component C (17 steps, 676 CP, 4704+ craftsmanship, 4537+ control)
Rotation on Teamcraft: https://ffxivteamcraft.com/simulator/36327/34619/wppQdj8RRg72pqyoJzDO

Macro 1
```
/ac "Reflect" <wait.3>
/ac "Waste Not II" <wait.2>
/ac "Manipulation" <wait.2>
/ac "Innovation" <wait.2>
/ac "Preparatory Touch" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Innovation" <wait.2>
/ac "Preparatory Touch" <wait.3>
/ac "Great Strides" <wait.2>
/ac "Byregot's Blessing" <wait.3>
/ac "Veneration" <wait.2>
/ac "Waste Not" <wait.3>
/ac "Groundwork" <wait.3>
```
Macro 2
```
/ac "Groundwork" <wait.3>
/ac "Groundwork" <wait.3>
```