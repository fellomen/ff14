# Crafting macros for 6.0 recipes
All macros tested with 3188 Craftsmanship and 3151 control.

## 1-button Collectables, 560 CP
At 3188 craftsmanship it's just low enough to not finish the craft (Lv 89) at the `Delicate Synthesis` step. If you have more craftsmanship you might want to test first if it's safe to use and maybe opt for a `Hasty Touch` or other quality skill.
For lower level collectables you'll very likely have to replace the `Delicate Synthesis`.
```
/ac "Muscle Memory" <wait.3>
/ac "Veneration" <wait.3>
/ac "Manipulation" <wait.3>
/ac "Waste Not II" <wait.3>
/ac "Groundwork" <wait.3>
/ac "Delicate Synthesis" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Innovation" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Great Strides" <wait.3>
/ac "Byregot's Blessing" <wait.3>
/ac "Groundwork" <wait.3>
```


Note these aren't particularly good macros, but they work good enough for what I want (mainly only using 1 macro per craft). Not tested for 6.1 recipes.

## 6.0 2★ 35 durability
### 15 steps, 616 CP required, ~7k quality (85% HQ chance)
```
/ac "Muscle Memory" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Veneration" <wait.3>
/ac "Master's Mend" <wait.3>
/ac "Manipulation" <wait.3>
/ac "Groundwork" <wait.3>
/ac "Waste Not II" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Innovation" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Great Strides" <wait.3>
/ac "Byregot's Blessing" <wait.3>
/ac "Groundwork"
```

### 16 steps, 605 CP required, ~7k quality (85% HQ chance)
- Requires 1 additional `Basic Synthesis` after the macro completes.
```
/ac "Muscle Memory" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Veneration" <wait.3>
/ac "Master's Mend" <wait.3>
/ac "Manipulation" <wait.3>
/ac "Groundwork" <wait.3>
/ac "Waste Not II" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Innovation" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Great Strides" <wait.3>
/ac "Byregot's Blessing" <wait.3>
/ac "Careful Synthesis"
```


## 6.0 2★ 70 durability

### 15 steps, 546CP required, ~6.5k quality (meant to be used with HQ mats)
```
/ac "Muscle Memory" <wait.3>
/ac "Veneration" <wait.3>
/ac "Manipulation" <wait.3>
/ac "Waste Not II" <wait.3>
/ac "Groundwork" <wait.3>
/ac "Groundwork" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Great Strides" <wait.3>
/ac "Innovation" <wait.3>
/ac "Byregot's Blessing" <wait.3>
/ac "Groundwork"
```

### 19 steps, 588CP required, ~8-10k quality (~60-90% HQ chance) depending on procs
Macro 1
```
/macrolock
/ac "Reflect" <wait.3>
/ac "Manipulation" <wait.3>
/ac "Innovation" <wait.3>
/ac "Waste Not" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Innovation" <wait.3>
/ac "Trained Finesse" <wait.3>
/ac "Great Strides" <wait.3>
/ac "Byregot's Blessing"
/echo "macro 1 done" <se.6>
```
Macro 2
```
/macrolock
/ac "Waste Not" <wait.3>
/ac "Veneration" <wait.3>
/ac "Groundwork" <wait.3>
/ac "Groundwork" <wait.3>
/ac "Groundwork" <wait.3>
/ac "Groundwork" <wait.3>
```

# Crafting macros for 6.2 recipes
All macros tested with 3849 Craftsmanship and 3329 control. Those values are reachable with full Perfectionist's(gear, MH, OH) and `Calamari Ripieni` HQ food. But you're going to need some overmelded i560 accessories.

For the most part I'm still using my [6.0 macros](https://gitlab.com/fellomen/ff14/-/blob/main/macros/Crafting%206.0.md).

Depending on your craftsmanship you might want to slightly adjust the 1-button 70 durability macro: Replace 1 `Preparatory Touch` with `Delicate Synthesis`:
```
/ac "Muscle Memory" <wait.3>
/ac "Veneration" <wait.3>
/ac "Manipulation" <wait.3>
/ac "Waste Not II" <wait.3>
/ac "Groundwork" <wait.3>
/ac "Groundwork" <wait.3>
/ac "Delicate Synthesis" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Innovation" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Great Strides" <wait.3>
/ac "Byregot's Blessing" <wait.3>
/ac "Groundwork"
```

I have not tried the 19-step macro, but it should still work too.


# Macros for crafting Splendorous Tools steps

Some notes about using these macros.
- I usually aim for 2 criteria on my macros:
  - Usable with old/welfare gear (ie, in 6.35 I'm using perfectionist (left side + OH) and integral (right side)).
  - Fits into one macro
- This often leads to the need to use HQ ingredients to help boost quality, this is mentioned on the macros that require them.
- I've also listed some extra control thresholds that can prevent issues if specific steps in the macro are done on a poor condition. I've also included thresholds for Byregot's Blessing on poor condition, but I'm not sure if these are reachable currently.

## Step 1 - A Dedicated Tool / Patch 6.35 / i590
Requirements / alternate thresholds: 
| CP  | Craftsmanship | Control | Notes                                                                                           |
| :-: |           :-: |     :-: | :--                                                                                             |
| 542 |          3618 |    3355 | Bare minimum control to reach 9,000+ quality without any procs.                                 |
|     |               |    3393 | Prevents a failed craft, if `Delicate Synthesis` is done on a poor condition.                   |
|     |               |    3637 | Prevents a failed craft, if `Preparatory Touch` after `Innovation` is done on a poor condition. |
|     |               |    4385 | Prevents a failed craft, if `Byregot's Blessing` is done on a poor condition.                   |
| 560 |          3470 |         | Replacing the last `Basic Synthesis` with `Groundwork` lowers the required craftsmanship.       |


```
/ac "Muscle Memory" <wait.3>
/ac "Veneration" <wait.2>
/ac "Manipulation" <wait.2>
/ac "Waste Not II" <wait.2>
/ac "Groundwork" <wait.3>
/ac "Delicate Synthesis" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Innovation" <wait.2>
/ac "Preparatory Touch" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Great Strides" <wait.2>
/ac "Byregot's Blessing" <wait.3>
/ac "Basic Synthesis"
```

## Step 2 - An Adaptive Tool / Patch 6.35 / i620
Requirements / additional thresholds: 
|  CP | Craftsmanship | Control | Notes |
| :-: |           :-: |     :-: | :--   |
| 552 |          3675 |    3486 | Baseline stats to reach 11,000+ quality with **full HQ ingredients** and no procs.              |
|     |               |    3542 | To prevent a fail crafted, if the first `Delicate Synthesis` is done on a poor condition.       | 
|     |               |    3787 | Prevents a failed craft, if `Preparatory Touch` after `Innovation` is done on a poor condition. |
|     |               |    4642 | Prevents a failed craft, if `Byregot's Blessing` is done on a poor condition.                   |

```
/ac "Muscle Memory" <wait.3>
/ac "Veneration" <wait.2>
/ac "Manipulation" <wait.2>
/ac "Waste Not II" <wait.2>
/ac "Groundwork" <wait.3>
/ac "Delicate Synthesis" <wait.3>
/ac "Delicate Synthesis" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Innovation" <wait.2>
/ac "Preparatory Touch" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Great Strides" <wait.2>
/ac "Byregot's Blessing" <wait.3>
/ac "Groundwork"
```

Since I wanted to keep the macro short, I'm using HQ materials to reduce the amount of quality needed. So here's a macro for crafting 81-90 recipes in HQ, requires 420 CP.
```
/mcancel
/mlock
/ac "Reflect" <wait.3>
/ac "Manipulation" <wait.3>
/ac "Waste Not II" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Byregot's Blessing" <wait.3>
/ac "Groundwork" <wait.3>
/ac "Groundwork"
```

## Step 3 - A Tool of Her Own / Patch 6.45 / i625
The same macro as for [An Adaptive Tool](#step-2-an-adaptive-tool-patch-635-i620) can be reused for this with higher craftsmanship stat: 4135+ required. (HQ items still required).

## Step 4 - A Tool without Compare / Patch 6.45 / i630
Requirements / additional thresholds: 
|  CP | Craftsmanship | Control | Notes |
| :-: |           :-: |     :-: | :--   |
| 524 |          4070 |    3508 | Baseline stats to reach 13,000+ quality with **full HQ ingredients** and no procs.              |
|     |               |    3738 | To prevent a fail crafted, if the first `Preparatory Touch` is done on a poor condition.       | 
|     |               |    4559 | Prevents a failed craft, if `Byregot's Blessing` is done on a poor condition.                   |
```
/ac "Muscle Memory" <wait.3>
/ac Veneration <wait.2>
/ac Manipulation <wait.2>
/ac "Waste Not II" <wait.2>
/ac Groundwork <wait.3>
/ac Groundwork <wait.3>
/ac Innovation <wait.2>
/ac "Preparatory Touch" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac Innovation <wait.2>
/ac "Great Strides" <wait.2>
/ac "Byregot's Blessing" <wait.3>
/ac Groundwork <wait.3>
```

