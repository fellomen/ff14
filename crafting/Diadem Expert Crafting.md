# Diadem expert crafting titles

This was originally available as a snippet [at this link](https://gitlab.com/fellomen/ff14/-/snippets/2419812).

## Material list

A list containing all Diadem materials requires for 100 expert crafts of each phase for each crafter.

https://garlandtools.org/db/#list/cGOadlraC5

Crafting those will unlock (for each crafter):
- `The Height of <Class>` (Title: `The Nest's Own <Class>`): 100 phase 2 expert recipes
- `<Class> of a Feather` (Title: `Featherfall's Finest <Class>`): 100 phase 3 expert recipes
- `An Ode to <Class>` (Title: `The Risensung <Class>`): 100 phase 4 expert recipes
- `Skyward <Class> I`: 50,000+ skyward score for the class. If you manage to get 1400+ collectability on at least half of the phase 4 expert crafts.
- `Crafting in the Air` (Minion: `Laladile`): 50,000+ skyward score on any class.

Expert craft payouts (per crafter)
| Phase | Scrips        | Points           | Kupo stamps             |
| :-:   | --:           | :-:              | :-:                     |
| 4     | 6.600 ~ 8.000 | 17.500 ~  82.000 | 100 -> 10x Kupo Voucher |
| 3     |   100 ~   300 |  2.400 ~  11.000 | 0                       |
| 2     |   100 ~   300 |  2.400 ~   8.000 | 0                       |
|       |               |                  |                         |
| Total | 6.800 ~ 8.600 | 22.300 ~ 101.000 | 100 -> 10x Kupo Voucher |

## Macros (as of 7.05)
Rotation folder on Teamcraft: https://ffxivteamcraft.com/rotation-folder/LL0jwrdK4polmXq7fM5Y

Individual Macros have a _rotation on Teamcraft_ link below.

## Recommended macros
### Grade 2 (14 steps, 522 CP, 3724+ craftsmanship, 3991+ control) [based on Variant 1](#base-variant-1), [rotation on Teamcraft](https://ffxivteamcraft.com/simulator/29832/33753/uAjK3F7mxbKiLfkU0D3B)
```
/macrolock
/ac "Reflect" <wait.3>
/ac "Veneration" <wait.2>
/ac "Waste Not II" <wait.2>
/ac "Groundwork" <wait.3>
/ac "Groundwork" <wait.3>
/ac "Preparatory Touch"" <wait.3>
/ac "Immaculate Mend" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Innovation" <wait.2>
/ac "Preparatory Touch" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Great Strides" <wait.2>
/ac "Byregot's Blessing" <wait.3>
/ac "Groundwork" <wait.3>
```

### Grade 3 (16 steps, 6880 CP, 4718+ craftsmanship, 4524+ control) [based on Variant 2](#base-variant-2), [rotation on Teamcraft](https://ffxivteamcraft.com/simulator/31224/34415/lcnKo3nQbwFmjPvagWdP)
Requires 1 more `Groundwork` after the macro finishes
```
/ac "Reflect" <wait.3>
/ac "Waste Not II" <wait.2>
/ac "Preparatory Touch" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Delicate Synthesis" <wait.3>
/ac "Delicate Synthesis" <wait.3>
/ac "Immaculate Mend" <wait.3>
/ac "Innovation" <wait.2>
/ac "Preparatory Touch" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Great Strides" <wait.2>
/ac "Byregot's Blessing" <wait.3>
/ac "Immaculate Mend" <wait.3>
/ac "Veneration" <wait.2>
/ac "Groundwork" <wait.3>
```

### Grade 4 (16 steps, 666 CP, 4715+ craftsmanship, 4667+ control) [based on Variant 2](#base-variant-2), [rotation on Teamcraft](https://ffxivteamcraft.com/simulator/31953/34473/fwm4d2mblBemUEd9UeRw)
Requires 1 more `Groundwork` after the macro finishes
```
/ac "Reflect" <wait.3>
/ac "Waste Not II" <wait.2>
/ac "Preparatory Touch" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Delicate Synthesis" <wait.3>
/ac "Immaculate Mend" <wait.3>
/ac "Innovation" <wait.2>
/ac "Preparatory Touch" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Great Strides" <wait.2>
/ac "Byregot's Blessing" <wait.3>
/ac "Immaculate Mend" <wait.3>
/ac "Veneration" <wait.2>
/ac "Groundwork" <wait.3>
/ac "Groundwork" <wait.3>
```

## More detailed macro descriptions to satisfy my brainworms

### Base Variant 1
```
/ac "Reflect" <wait.3>
/ac "Veneration" <wait.2>
/ac "Waste Not II" <wait.2>
/ac "Groundwork" <wait.3>
/ac "Groundwork" <wait.3>
/ac "Delicate Synthesis" <wait.3>
/ac "Preparatory Touch"" <wait.3>
/ac "Immaculate Mend" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Innovation" <wait.2>
/ac "Preparatory Touch" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Great Strides" <wait.2>
/ac "Byregot's Blessing" <wait.3>
/ac "Groundwork" <wait.3>
```
- Notes/adjustments for:
  - Grade 2:
    - Remove the `Delicate Synthesis` to avoid overshooting progress.    
  - Grade 3:
    - No working implementation made yet.
  - Grade 4:
    - Has some risk attached to it as a one (or two) `Malleable` condition(s) on the first two `Groundwork`s can make you prematurely finish the craft. To reduce/prevent this, you can do: 
      - Add a `Final Appraisal` before the **Delicate Synthesis**. (16 steps, +1 CP, prevents 1 Malleable attack)
      - Additonally add another `Final Appraisal` before the **second** `Groundwork`. (17 steps, +2 CP, prevents 2 Malleable attack)

### Base Variant 2
```
/ac "Reflect" <wait.3>
/ac "Waste Not II" <wait.2>
/ac "Preparatory Touch" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Delicate Synthesis" <wait.3>
/ac "Immaculate Mend" <wait.3>
/ac "Innovation" <wait.2>
/ac "Preparatory Touch" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Great Strides" <wait.2>
/ac "Byregot's Blessing" <wait.3>
/ac "Immaculate Mend" <wait.3>
/ac "Veneration" <wait.2>
/ac "Groundwork" <wait.3>
/ac "Groundwork" <wait.3>
```
- Notes/adjustments for:
  - Grade 2:
    - 15 steps, 648 CP, 4368+ craftsmanship, 3718+ control
    - If you have more than 4540 control, you can remove one of the first two `Preparatory Touch` to reduce step count to 14 and CP cost to 626 
  - Grade 3:
    - 16 steps, 6880 CP, 4718+ craftsmanship, 4524+ control
    - Add an additional `Delicate Synthesis` after the first one. The last `Groundwork` has to be pressed additionally after the macro.
  - Grade 4:
    - 16 steps, 666 CP, 4715+ craftsmanship, 4667+ control
    - Requires one additional `Groundwork` step after the macro.
    - Has a higher CP requirement, but is safe against `Malleable` attacks without the need to use `Final Appraisal`.