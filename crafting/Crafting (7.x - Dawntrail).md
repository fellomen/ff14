# Crafting macros for 7.0

## Patch 7.05 crafted DoW/M gear

These are made with budget gear:
- MH and OH are crafted 7.0 white gear, with only 1 craftsmanship and 1 control tier X meld.
- Left-side and right-side gear are full crafted 7.0 white gear, melds and overmelds with tier X and IX materia.
- Used food: HQ Tsai tou Vounou (35 dura), and HQ Rroneek Steak (70 durability).

### 35 durability mats, 23 steps, 656 CP
- Craftsmanship: 4879 (shouldn't be much higher, or you'll have to move around some progress steps not prematurely finish them)
- Control: 4689 (reaches ~10k quality, ~80% HQ chance, use with HQ mats)
- CP: 656
  - More CP can be used on extra `Trained Finesse` steps at the end

Macro 1
```
/macrolock
/ac "Muscle Memory" <wait.3>
/ac "Veneration" <wait.3>
/ac "Groundwork" <wait.3>
/ac "Manipulation" <wait.3>
/ac "Waste Not II" <wait.3>
/ac "Delicate Synthesis" <wait.3>
/ac "Delicate Synthesis" <wait.3>
/ac "Basic Touch" <wait.3>
/ac "Standard Touch" <wait.3>
/ac "Advanced Touch" <wait.3>
/ac "Basic Touch" <wait.3>
/ac "Standard Touch" <wait.3>
/ac "Advanced Touch" <wait.3>
/echo Macro #1 of 2 finished (DT 7.05 2★, 35 dura, 656 CP) <se.6>
```
Macro 2
```
/macrolock
/ac "Master's Mend" <wait.3>
/ac "Trained Perfection" <wait.3>
/ac "Basic Touch" <wait.3>
/ac "Standard Touch" <wait.3>
/ac "Advanced Touch" <wait.3>
/ac "Innovation" <wait.3>
/ac "Trained Finesse" <wait.3>
/ac "Great Strides" <wait.3>
/ac "Byregot's Blessing" <wait.3>
/ac "Basic Synthesis" <wait.3>
/echo Craft finished (DT 7.05 2★, 35 dura, 656 CP) <se.6>
```
### 70 durability crafts, 29 steps, 683 CP
- Craftsmanship: 4879 (shouldn't be much higher, or you'll have to move around some progress steps not prematurely finish them)
- Control: 4689 (reaches ~80% HQ chance)
- CP: 683
  - If you have more CP you can replace the `Hasty Touch`es in Macro 2 with other quality steps.
  - Or you can add more `Trained Finesse` at the end.

Macro 1
```
/ac "Muscle Memory" <wait.3>
/ac "Manipulation" <wait.3>
/ac "Waste Not II" <wait.3>
/ac "Veneration" <wait.3>
/ac "Groundwork" <wait.3>
/ac "Groundwork" <wait.3>
/ac "Groundwork" <wait.3>
/ac "Groundwork" <wait.3>
/ac "Basic Touch" <wait.3>
/ac "Standard Touch" <wait.3>
/ac "Advanced Touch" <wait.3>
/ac "Basic Touch" <wait.3>
/ac "Standard Touch" <wait.3>
/ac "Advanced Touch" <wait.3>
/echo Macro #1 of 2 finished (DT 7.05 2★, 70 dura, 683 CP) <se.6>
```
Macro 2
```
/ac "Trained Perfection" <wait.3>
/ac "Basic Touch" <wait.3>
/ac "Refined Touch" <wait.3>
/ac "Immaculate Mend" <wait.3>
/ac "Prudent Touch" <wait.3>
/ac "Hasty Touch" <wait.3>
/ac "Hasty Touch" <wait.3>
/ac "Hasty Touch" <wait.3>
/ac "Hasty Touch" <wait.3>
/ac "Hasty Touch" <wait.3>
/ac "Innovation" <wait.3>
/ac "Trained Finesse" <wait.3>
/ac "Great Strides" <wait.3>
/ac "Byregot's Blessing" <wait.3>
/ac "Basic Synthesis" <wait.3>
```

## Patch 7.0 purple scrip 1-button macros
### Variant 1a - 494 CP, 4750+ craftsmanship, 4381+ control
- Craftsmanship: 4750~5176
- Control: 4381+
- CP: 494
```
/ac "Muscle Memory" <wait.3>
/ac "Veneration" <wait.3>
/ac "Groundwork" <wait.3>
/ac "Delicate Synthesis" <wait.3>
/ac "Delicate Synthesis" <wait.3>
/ac "Delicate Synthesis" <wait.3>
/ac "Delicate Synthesis" <wait.3>
/ac "Immaculate Mend" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Innovation" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Great Strides" <wait.3>
/ac "Byregot's Blessing" <wait.3>
/ac "Groundwork" <wait.3>
```
### Variant 1b - 487 CP, 5100+ craftsmanship, 4381+ control
- Craftsmanship: 5100~5577
- Control: 4381+
- CP: 487

Variation of Variant 1a: replaces the fourth `Delicate Synthesis` with a `Prudent Touch` to increase the craftsmanship window to 5100~5577, and reduces CP cost by 7.

```
/ac "Muscle Memory" <wait.3>
/ac "Veneration" <wait.3>
/ac "Groundwork" <wait.3>
/ac "Delicate Synthesis" <wait.3>
/ac "Delicate Synthesis" <wait.3>
/ac "Delicate Synthesis" <wait.3>
/ac "Prudent Touch" <wait.3>
/ac "Immaculate Mend" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Innovation" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Great Strides" <wait.3>
/ac "Byregot's Blessing" <wait.3>
/ac "Groundwork" <wait.3>
```

### Variant 2 - 546 CP, 4977+ craftsmanship, 4410+ control
Needs overall higher stats, but works with higher craftsmanship windows.
- Craftsmanship: 4977~6028
- Control: 4410
- CP: 546

```
/ac "Muscle Memory" <wait.3>
/ac Veneration <wait.2>
/ac Manipulation <wait.2>
/ac "Waste Not II" <wait.2>
/ac Groundwork <wait.3>
/ac Groundwork <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac Innovation <wait.2>
/ac "Preparatory Touch" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Great Strides" <wait.2>
/ac "Byregot's Blessing" <wait.3>
/ac Groundwork <wait.3>
```

## Leveling 91-100
Macros I use for leveling for leveling from 91 to 100 with the following stats[*](/img/dt_doh_leveling_gear.png):
- Craftsmanship: 3940
- Control:       3772
- CP:             532 (use food)

### Lv 81-90 mats HQ (400 CP)
```
/ac "Reflect" <wait.3>
/ac "Manipulation" <wait.3>
/ac "Waste Not II" <wait.3>
/ac "Basic Touch" <wait.3>
/ac "Standard Touch" <wait.3>
/ac "Advanced Touch" <wait.3>
/ac "Basic Touch" <wait.3>
/ac "Standard Touch" <wait.3>
/ac "Advanced Touch" <wait.3>
/ac "Great Strides" <wait.3>
/ac "Byregot's Blessing" <wait.3>
/ac "Groundwork" <wait.3>
/ac "Groundwork" <wait.3>
```
### 91-100 40 durability
#### Version 1 - good up around to Lv 96 - 520 CP
```
/ac "Reflect" <wait.3>
/ac "Manipulation" <wait.3>
/ac "Innovation" <wait.3>
/ac "Waste Not" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Great Strides" <wait.3>
/ac "Innovation" <wait.3>
/ac "Byregot's Blessing" <wait.3>
/ac "Veneration" <wait.3>
/ac "Waste Not" <wait.3>
/ac "Groundwork" <wait.3>
/ac "Groundwork" <wait.3>
```
#### Version 2 - more progress (needed at Lv 97+) - 576 CP
```
/ac "Reflect" <wait.3>
/ac "Manipulation" <wait.3>
/ac "Innovation" <wait.3>
/ac "Waste Not" <wait.3>
/ac "Delicate Synthesis" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Delicate Synthesis" <wait.3>
/ac "Great Strides" <wait.3>
/ac "Innovation" <wait.3>
/ac "Byregot's Blessing" <wait.3>
/ac "Master's Mend" <wait.3>
/ac  "Veneration" <wait.3>
/ac "Groundwork" <wait.3>
```
\+ 1 additional Groundwork or other progress ability


### 91-100 80 durability
#### Version 1 - good up around to Lv 96 - 546 CP
```
/ac "Muscle Memory" <wait.3>
/ac "Manipulation" <wait.3>
/ac "Waste Not II" <wait.3>
/ac "Veneration" <wait.3>
/ac "Groundwork" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Great Strides" <wait.3>
/ac "Innovation" <wait.3>
/ac "Byregot's Blessing" <wait.3>
/ac "Veneration" <wait.3>
/ac "Groundwork" <wait.3>
```
#### Version 2 - more progress (needed at Lv 97+) - 600 CP
Macro 1. If you need just a little more progress you can swap the first `Preparatory Touch` for a `Delicate Synthesis` to get some more progress  and 8 saved CP at the cost of less quality.
```
/ac "Muscle Memory" <wait.3>
/ac "Manipulation" <wait.3>
/ac "Waste Not II" <wait.3>
/ac "Veneration" <wait.3>
/ac "Groundwork" <wait.3>
/ac "Groundwork" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Innovation" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Preparatory Touch" <wait.3>
/ac "Great Strides" <wait.3>
/ac "Innovation" <wait.3>
/ac "Byregot's Blessing" <wait.3>
```
Macro 2
```
/ac "Veneration" <wait.3>
/ac "Prudent Synthesis" <wait.3>
/ac "Prudent Synthesis" <wait.3>
/ac "Basic Synthesis" <wait.3>
```