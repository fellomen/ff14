import java.awt.Robot
import java.awt.event.KeyEvent
import kotlin.random.Random

/** Minimum time between each execution, in milliseconds, default 1 minute */
val MIN_INTERVAL = 60_000
/**
 * Maximum time between each execution, in milliseconds, default 20 minutes. Note the AFK kick timer
 * is 30 minutes currently.
 */
val MAX_INTERVAL = 60_000 * 20
/**
 * The programm will press any of these buttons at random intervals. If you changed keybinds for
 * your menues you might have to change them, or you can add additional keybinds. Note that
 * shift/control/alt modifiers are not currently supported.
 *
 * For normal letter or digit keys you can just add a KeyEvent.VK_<letter> to the list. For more
 * values you can check: https://docs.oracle.com/javase/7/docs/api/java/awt/event/KeyEvent.html
 */
val availableButtons =
        arrayOf(
                KeyEvent.VK_C, // Character menu
                KeyEvent.VK_B, // Gathering Log
                KeyEvent.VK_N, // Crafting Log
                KeyEvent.VK_W, // Movement Forward
                KeyEvent.VK_A, // Movement Left
                KeyEvent.VK_S, // Movement Backward
                KeyEvent.VK_D, // Movement Right
        )

fun main() {
    val robot = Robot()
    while (true) {
        robot.autoDelay = Random.nextInt(200, 1200)
        for (i in 1..3) {
            val button = availableButtons.random()
            robot.keyPress(button)
            robot.keyRelease(button)
        }
        Thread.sleep(Random.nextInt(MIN_INTERVAL, MAX_INTERVAL).toLong())
    }
}
