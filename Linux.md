
## Various Troubleshooting

<details>
<summary>[ffmt](https://github.com/fosspill/FFXIV_Modding_Tool) error message: Process terminated. Couldn't find a valid ICU package installed on the system. Set the configuration flag System.Globalization.Invariant to true if you want to run with no globalization support.</summary>

Add `System.Globalization.Invariant": true` to the `configProperties` in the `ffmt.runtimeconfig.json` file.

Should look like this afterwards:
```json
{
  "runtimeOptions": {
    "tfm": "netcoreapp3.1",
    "includedFrameworks": [
      {
        "name": "Microsoft.NETCore.App",
        "version": "3.1.21"
      }
    ],
    "configProperties": {
      "System.Reflection.Metadata.MetadataUpdater.IsSupported": false,
      "System.Globalization.Invariant": true
    }
  }
}
```

</details>
