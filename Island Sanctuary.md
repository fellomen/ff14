# Island Santuary

Just a place to dump my thoughts and notes.

## Protip
* When you unlock granaries first send them out to collect a total of 6 `Sanctuary Spruce Logs` and 9 `Sanctuary Raw Garnet`. These mats are required to renovate your Granaries and Workshops (respectively) from tier 2 to tier 3 and are not obtainable elsewhere.
* ~~You can't release an animal when you've put it into the care of the caretaker. When you capture an animal while all your pen slots are full and all of them are given to the caretaker you will not be able to release any of them. Therefore having to release the freshly caught animal. Make sure to end the caretaking service on an animal before attempting to catch a new animal.~~ This issue was fixed in patch 6.3: [link to patch notes](https://eu.finalfantasyxiv.com/lodestone/topics/detail/2ebebcdeedfecd2af0bf4cd5ce2d707e35f50d70)
* ~~Achievement hunters: If you let your caretaker/gardener take care of your animals/plants and you let them stock up multiple days worth of leavings/produce, you will only get **one** point of progress for their respective achievements. So still make sure you collect them whenever they are ready.~~ This issue was fixed in patch 6.28: [link to patch notes](https://na.finalfantasyxiv.com/lodestone/topics/detail/c8900c4aae544f7a013a49553aa104c1961a5c87)

Use this comprehensive guide by Leiton Grey (Cactuar) for more info, also has a link to the discord: [Island Sanctuary Comprehensive Guide](https://docs.google.com/spreadsheets/d/1yAfkO_OQNXexkSFGo3oABaIFmC83rqdejyuhs_dboHM/htmlview)

## Reconstructing landmarks
You can (instantly) reconstruct landmarks for diminished island EXP once you have one built on a landmark spot.
| Landmark             | EXP | Materials | Ratio | Rank |
| :--                  | :-- | :--       | :--   |      |
| Quixotic Windmill    | 135 | 50        | 2.7   |      |
| Towering Tree Fort   | 135 | 50        | 2.7   |      |
| Boiling Bathhouse    | 180 | 53        | 3.4   |      |
| Lominsan Lighthouse  | 285 | 53 ³ᴿ     | 5.4   |      |
| Water Otter Fountain | 285 | 53 ³ᴿ     | 5.4   |      |
| Gamboling Garden     | 285 | 55 ⁵ᴿ     | 5.2   |      |
| Quaint Clock Tower   | 285 | 55        | 5.2   | 15   |

ˣᴿ .. contains x rare materials

So the Windmill/Tree Fort are slightly worse than spamming `Island Sweetfeed` and `Makeshift Nets` crafts at 3.3 XP (10 EXP for 3 materials). The Boiling Bathhouse is slightly better. In addition you'll have to cycle between the Bathhouse and one 135 XP landmark, so you'll come out to a total of 315 EXP for 103 materials for 3.05 EXP per material, making it effectively worse.

**Update for 6.4**: Cycling between Quaint Clock Tower and Boiling Bathhouse gets you a 4.3 EXP/material ratio, making it better than simple crafting.

Also note that this doesn't take into account how fast materials are gatherable and item amount limitations for normal crafts.


## The Cropland / Seeds & Produce
- Plots need to be at `Soaked`, `Wet`, or `Moist` to grow.
  - `Soaked`: 0-6h after watering.
  - `Wet`: 6-24h after watering.
  - `Moist`: 24-36h after watering
  - `Dry`: 36h+ after watering
- When it's raining on the island your plots will automatically fill up their moisture level, but you have to be on the island when it rains for it to apply.

### Seed strategies (6.2 + 6.3)
These strategies are for the amount of seeds we had in 6.2.
- Diversify seeds for better material availability for the workshop.
  - As of 6.2 there's enough space to plant 2 seeds of each type of produce.
  - **Update for 6.3**: Once you reach rank 11, you'll have to replace two seeds with the new seeds.
- Comfy maxing for Premium Greenfeed
  - Fill your croplands with only 2 types of seed (so 10 seeds per produce).
  - Allows for easier creation of Premium Greenfeed since you don't have to switch materials between crafts.
  - Bonus comfy points: Buy the seeds off the gardener so you don't have to go and gather some yourself.
- Combination of the above:
  - Plant 1 of each seed type: Fills up 10 of your 20 slots.
  - Fill up the remaining 10 slots split between 2 seed types. (2x 5 seeds). Which ones doesn't matter, they'll be used to produce Premium Greenfeed for your pasture.
  - Every 2 days this will yield:
    - 5 produce of each seed for use in the workshop
    - 25 produce (x2) to be crafted into Premium Greenfeed (36 Premium Greenfeed). You might have to use some of the other produce as well to create Premium Greenfeed to keep up with food requirements on a full pasture.
  - During my 6.2 playing this was a net-positive still and I never ended up too short on produce or feed with this strategy.
  - **Update for patch 6.3**: I replaced one of the 5-slot (so now they are 4 slots per seed) with one of the new seed each. I didn't pay too close attention, but it seems to still work out.
  - **Update for patch 6.4**: Replaced 2 more of the 2x 4-slot seeds with the new seeds (starting from rank 14). It probably won't that be "comfortable" anymore and it's very similar to the diversify strategy now (18 slots on unique seeds + 2 slots for duplicate seeds).


## Spamming nodes/crafts for EXP
I did not try their hourly rates, but some routes that can be used for effective material gathering:

### Apples/Vines + Branches/Logs
With this route you'll be collecting all materials to craft `Island Sweetfeed` and `Makeshift Nets` at the same time.

1. Start at the apple tree `X: 18.0 / Y" 26.7` and follow the river eastward, collect from all apple and normal trees until you reach the `X: 23.5 / Y: 27.0` apple tree.
2. Cross the river to the north and collect one from one apple tree (`X: 23.6 / Y: 25.6`) and 3 normal trees.
3. Follow the river back westward and collect from apple trees and normal tress along the way until you reach the apple tree at `X: 19.6 / Y: 24.5`.
4. Return to the starting position and repeat.

![img/apple_route.png](img/island_sanctuary/apple_route.png)
* Note: The dots are not the actual collectable nodes in this picture and are just meant as guidelines for the route.
### Alternative: Apple trees only
1. You can use the same route as mentioned above and only collect from apple trees.
2. When returning to the starting position collect one `Pumpkin Seed` (or any other node) to start the loop again.

### Logs + Sap/Branches
A route containing 11 tree nodes for logs.
- Red dots are Log/Sap nodes.
- Blue dots are Log/Branch nodes.
- You may take the alternate route (grey line) if you prefer 2 extra branches over 2 saps, all though the route gets slightly longer.
- Alternatively there's also easily accessible 2 palm trees (Palm Log/Palm Leaf) and 3~4 accessible apple trees (Apple/Vine) along the route.

![img/log_route.png](img/island_sanctuary/log_route.png)

# DAMN BRAT
![DAMN BRAT](img/island_sanctuary/DAMN_BRAT.png)
